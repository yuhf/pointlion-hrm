package com.pointlion.back.oa.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pointlion.common.annotation.Log;
import com.pointlion.common.core.controller.BaseController;
import com.pointlion.common.core.domain.AjaxResult;
import com.pointlion.common.enums.BusinessType;
import com.pointlion.back.oa.domain.ActHiTaskinst;
import com.pointlion.back.oa.service.ActHiTaskinstService;
import com.pointlion.common.utils.poi.ExcelUtil;
import com.pointlion.common.core.page.TableDataInfo;

/**
 * 历史任务Controller
 * 
 * @author pointLion
 * @date 2023-05-14
 */
@RestController
@RequestMapping("/oa/actHiTaskInst")
public class FlowHiTaskinstController extends BaseController
{
    @Autowired
    private ActHiTaskinstService actHiTaskinstService;

    /**
     * 查询历史任务列表
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActHiTaskinst actHiTaskinst)
    {
        startPage();
        List<ActHiTaskinst> list = actHiTaskinstService.selectActHiTaskinstList(actHiTaskinst);
        return getDataTable(list);
    }

    /**
     * 导出历史任务列表
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:export')")
    @Log(title = "历史任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ActHiTaskinst actHiTaskinst)
    {
        List<ActHiTaskinst> list = actHiTaskinstService.selectActHiTaskinstList(actHiTaskinst);
        ExcelUtil<ActHiTaskinst> util = new ExcelUtil<ActHiTaskinst>(ActHiTaskinst.class);
        util.exportExcel(response, list, "历史任务数据");
    }

    /**
     * 获取历史任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(actHiTaskinstService.selectActHiTaskinstById(id));
    }

    /**
     * 新增历史任务
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:add')")
    @Log(title = "历史任务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActHiTaskinst actHiTaskinst)
    {
        return toAjax(actHiTaskinstService.insertActHiTaskinst(actHiTaskinst));
    }

    /**
     * 修改历史任务
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:edit')")
    @Log(title = "历史任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActHiTaskinst actHiTaskinst)
    {
        return toAjax(actHiTaskinstService.updateActHiTaskinst(actHiTaskinst));
    }

    /**
     * 删除历史任务
     */
    @PreAuthorize("@ss.hasPermi('oa:actHiTaskInst:remove')")
    @Log(title = "历史任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(actHiTaskinstService.deleteActHiTaskinstByIds(ids));
    }


    /*****
     * 获取任务执行次数
     * @return
     */
    @RequestMapping("/getTaskCishuReport")
    public TableDataInfo getTaskCishuReport(ActHiTaskinst actHiTaskinst){
        startPage();
        List<Map> list = actHiTaskinstService.getTaskCishuReport(actHiTaskinst);
        return getDataTable(list);
    }
}
